/*
   This file is part of the Nepomuk KDE project.
   Copyright (C) 2011 Vishesh Handa <handa.vish@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) version 3, or any
   later version accepted by the membership of KDE e.V. (or its
   successor approved by the membership of KDE e.V.), which shall
   act as a proxy defined in Section 6 of version 3 of the license.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "metadatasharing.h"
#include "dbusoperators_p.h"
#include "queryconnection.h"
#include "nso.h"

#include <KDebug>
#include <KServiceTypeTrader>

#include <QtCore/QUuid>

#include <Nepomuk/Resource>
#include <Nepomuk/ResourceManager>
#include <Nepomuk/Variant>

#include <Soprano/Model>
#include <Soprano/QueryResultIterator>

using namespace Nepomuk::Vocabulary;

Nepomuk::MetadataSharing::MetadataSharing( QObject* parent, const QList< QVariant >& )
    : Service(parent)
{
    kDebug();
    Query::registerDBusTypes();

    m_connectionCount = 0;

    // Make sure we have our own Nepomuk Repository identifier
    createRepository();

    loadAllPlugins();
}

Nepomuk::MetadataSharing::~MetadataSharing()
{
}

QDBusObjectPath Nepomuk::MetadataSharing::query(const QString& repositoryUri,
                                                const QString& query, const QDBusMessage& msg)
{
    kDebug() << repositoryUri;
    Query::Query q = Query::Query::fromString( query );
    if ( q.isValid() ) {
        // Get the correct Plugin and issue query
        //if( !m_testPlugin->canHandle( repositoryUri ) )
        //    return QDBusObjectPath();

        QueryReply* reply = m_testPlugin->query( repositoryUri, q );
        if( !reply )
            return QDBusObjectPath();

        // The DBus connection to relay back the results
        QueryConnection* con = new QueryConnection( reply, this );
        return con->registerDBusObject( msg.service(), m_connectionCount++ );
    }

    return QDBusObjectPath();
}

QDBusObjectPath Nepomuk::MetadataSharing::fetch(const QString& repositoryUri,
                                                const QString& resourceUri, const QDBusMessage& msg)
{
    //TODO: Check if repositoryUri is online

    MetadataReply* reply = m_testPlugin->fetch( repositoryUri, resourceUri );
    if( !reply )
        return QDBusObjectPath();

    // The DBus connection to relay back the statements
    //FIXME: Implement me!
    return QDBusObjectPath();
}

void Nepomuk::MetadataSharing::loadAllPlugins()
{
    KService::Ptr service = KService::serviceByDesktopName("nepomukmetadatasharing-testplugin");
    kDebug() << service->desktopEntryPath();

    KPluginFactory *factory = KPluginLoader( service->library() ).factory();
    m_testPlugin = factory->create<MetadataTransferPlugin>( this );

    if( !m_testPlugin ) {
        kDebug() << "Could not load the MetadataTransferPlugin Test";
        return;
    }
}

void Nepomuk::MetadataSharing::createRepository()
{
    // Check if it already exists
    QString query = QString::fromLatin1("ask where { ?r a %1 . ?r %2 ?ident . ?r %3 %4 }")
                    .arg( Soprano::Node::resourceToN3( NSO::Repository() ),
                          Soprano::Node::resourceToN3( NSO::repositoryIdentifier() ),
                          Soprano::Node::resourceToN3( NSO::belongsTo() ),
                          Soprano::Node::resourceToN3( QUrl("nepomuk:/myself") ) );

    Soprano::Model *model = Nepomuk::ResourceManager::instance()->mainModel();
    bool exists = model->executeQuery( query, Soprano::Query::QueryLanguageSparql ).boolValue();
    if( exists ) {
        kDebug() << "Own Repository resource exists";
        return;
    }

    // Create it
    Nepomuk::Resource repo;
    repo.addType( NSO::Repository() );
    repo.setProperty( NSO::repositoryIdentifier(), Variant(QUuid::createUuid().toString()) );
    repo.setProperty( NSO::belongsTo(), QUrl("nepomuk:/myself") );

    kDebug() << "Created repository resource with uri : " << repo.uri();
}


#include <kpluginfactory.h>
#include <kpluginloader.h>

NEPOMUK_EXPORT_SERVICE( Nepomuk::MetadataSharing, "nepomukmetadatasharing" )

#include "metadatasharing.moc"