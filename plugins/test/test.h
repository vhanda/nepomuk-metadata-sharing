/*
   This file is part of the Nepomuk KDE project.
   Copyright (C) 2011 Vishesh Handa <handa.vish@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) version 3, or any
   later version accepted by the membership of KDE e.V. (or its
   successor approved by the membership of KDE e.V.), which shall
   act as a proxy defined in Section 6 of version 3 of the license.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TEST_H
#define TEST_H

#include "metadatatransferplugin.h"
#include "server.h"

#include <Nepomuk/Query/Query>

#include <DNSSD/PublicService>
#include <DNSSD/RemoteService>

namespace Nepomuk {

    class Test : public MetadataTransferPlugin
    {
        Q_OBJECT
    public:
        Test( QObject * parent = 0, const QList<QVariant>& args = QList<QVariant>() );

        bool canHandle( const QUrl& uri );
        QueryReply* query( const QUrl& uri, const Query::Query& query );
        MetadataReply* fetch(const QUrl& uri, const QUrl& resourceUri);

    private Q_SLOTS:
        void published( bool pub );

        void addService(DNSSD::RemoteService::Ptr ptr);
        void delService(DNSSD::RemoteService::Ptr ptr);

    private:
        Server m_server;
        DNSSD::PublicService *m_dnsService;

        /// Maps the repo uri to its ip
        QHash<QUrl, QString> m_onlineRepos;

        QUrl repositoryForUuid( const QString& uuid );
    };

}

#endif // TEST_H
