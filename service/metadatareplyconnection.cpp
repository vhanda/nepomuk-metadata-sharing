/*
   This file is part of the Nepomuk KDE project.
   Copyright (C) 2011 Vishesh Handa <handa.vish@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) version 3, or any
   later version accepted by the membership of KDE e.V. (or its
   successor approved by the membership of KDE e.V.), which shall
   act as a proxy defined in Section 6 of version 3 of the license.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "nso.h"
#include "metadatareplyconnection.h"
#include "metadatareplyadaptor.h"

#include <KJob>
#include <KDebug>

#include <nepomuk/datamanagement.h>
#include <nepomuk/simpleresourcegraph.h>
#include <nepomuk/simpleresource.h>

using namespace Nepomuk::Vocabulary;

Nepomuk::MetadataReplyConnection::MetadataReplyConnection(Nepomuk::MetadataReply* reply,
                                                          const QUrl& repoUri, QObject* parent)
    : QObject(parent),
      m_reply(reply),
      m_repositoryUri(repoUri)
{
    connect( m_reply, SIGNAL(newResources(QList<Soprano::Statement>)),
             this, SLOT(newResources(QList<Soprano::Statement>)));
}

Nepomuk::MetadataReplyConnection::~MetadataReplyConnection()
{

}

QDBusObjectPath Nepomuk::MetadataReplyConnection::registerDBusObject(const QString& dbusClient, int id)
{
    new MetadataReplyAdaptor( this );

    // build the dbus object path from the id and register the connection as a Query dbus object
    const QString dbusObjectPath = QString::fromLatin1( "/metadata%1" ).arg( id );
    QDBusConnection::sessionBus().registerObject( dbusObjectPath, this );

    // watch the dbus client for unregistration for auto-cleanup
    m_serviceWatcher = new QDBusServiceWatcher( dbusClient,
                                                QDBusConnection::sessionBus(),
                                                QDBusServiceWatcher::WatchForUnregistration,
                                                this );
    connect( m_serviceWatcher, SIGNAL(serviceUnregistered(QString)),
             this, SLOT(close()) );

    // finally return the dbus object path this connection can be found on
    return QDBusObjectPath( dbusObjectPath );
}

void Nepomuk::MetadataReplyConnection::newResources(const QList< Soprano::Statement >& stList)
{
    // Convert to SimpleResourceGraph
    SimpleResourceGraph graph;

    //FIXME: Add a fromStatementList
    //FIXME: They'll have some "nepomuk:/res/" uris, we need to convert them into blank uris
    foreach( const Soprano::Statement &st, stList ) {
        graph.addStatement( st );
    }

    // FIXME: Shouldn't the calling app be set over here? Instead of "nepomukmetadatasharing" as
    // the app
    QHash<QUrl, QVariant> additionalMetadata;
    additionalMetadata.insert( NSO::belongsTo(), m_repositoryUri );

    KJob *job = Nepomuk::storeResources( graph, IdentifyNew, NoStoreResourcesFlags, additionalMetadata );
    connect( job, SIGNAL(finished(KJob*)), this, SLOT(slotJobFinished(KJob*)) );
}

void Nepomuk::MetadataReplyConnection::slotJobFinished(KJob* job)
{
    if(job->error()) {
        kDebug() << job->errorString();
        return;
    }

    // Now how in the world do I get the resource Uri?
    // FIXME: Maybe storeResourceJob could provide the mappings?
    emit finished( QString() );
}

