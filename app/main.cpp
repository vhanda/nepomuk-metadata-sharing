/*
   This file is part of the Nepomuk KDE project.
   Copyright (C) 2011 Vishesh Handa <handa.vish@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) version 3, or any
   later version accepted by the membership of KDE e.V. (or its
   successor approved by the membership of KDE e.V.), which shall
   act as a proxy defined in Section 6 of version 3 of the license.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../service/dbusoperators_p.h"
#include "../lib/networkqueryserviceclient.h"

#include <Soprano/Model>
#include <Soprano/QueryResultIterator>

#include <Nepomuk/Resource>
#include <Nepomuk/ResourceManager>

#include <Nepomuk/Query/Query>
#include <Nepomuk/Query/QueryParser>
#include <Nepomuk/Query/Result>

// Vocabularies
#include <Soprano/Vocabulary/RDF>
#include <Soprano/Vocabulary/RDFS>
#include <Soprano/Vocabulary/NRL>
#include <Soprano/Vocabulary/NAO>

#include <Nepomuk/Vocabulary/NIE>
#include <Nepomuk/Vocabulary/NFO>
#include <Nepomuk/Vocabulary/NMM>
#include <Nepomuk/Vocabulary/NCO>
#include <Nepomuk/Vocabulary/PIMO>

#include <KAboutData>
#include <KCmdLineArgs>
#include <KLocale>
#include <KComponentData>

#include <QtCore/QCoreApplication>
#include <QtCore/QTimer>

#include <KUrl>
#include <KDebug>
#include <iostream>

using namespace Soprano::Vocabulary;
using namespace Nepomuk::Vocabulary;

class SharingApplication : public QCoreApplication {
    Q_OBJECT
public:
    SharingApplication(int& argc, char** argv);

private slots:
    void queryAllRepositories();

    void newEntries(const QList<Nepomuk::Query::Result>& results);
    void finishedListing();

private:
    QList<QUrl> m_repoList;
    int m_finished;
};

SharingApplication::SharingApplication(int& argc, char** argv)
    : QCoreApplication( argc, argv )
{
    Nepomuk::Query::registerDBusTypes();

    // FIXME: Use proper repository
    // Get all the repositories which donot belong to us FIXME: Why?
    QString query = QString::fromLatin1("select ?r where { ?r a nso:Repository . "
                                        "?r nso:repositoryIdentifier ?ident."
                                        "FILTER(!bif:exists((select * where { ?r nso:belongsTo <nepomuk:/myself> . })) )."
                                        "}");
    Soprano::Model *model = Nepomuk::ResourceManager::instance()->mainModel();
    Soprano::QueryResultIterator it = model->executeQuery( query, Soprano::Query::QueryLanguageSparql );
    while( it.next() ) {
        m_repoList << it[0].uri();
    }

    QTimer::singleShot( 0, this, SLOT(queryAllRepositories()) );
}

void SharingApplication::queryAllRepositories()
{
    const KCmdLineArgs *args = KCmdLineArgs::parsedArgs();
    QString queryString = args->arg( 0 );

    Nepomuk::Query::Query query = Nepomuk::Query::QueryParser::parseQuery( queryString );

    m_finished = 0;
    foreach( const QUrl& repo, m_repoList ) {
        Nepomuk::NetworkQueryServiceClient *nqsc = new Nepomuk::NetworkQueryServiceClient( repo, this );

        connect( nqsc, SIGNAL(newEntries(QList<Nepomuk::Query::Result>)),
                 this, SLOT(newEntries(QList<Nepomuk::Query::Result>)) );
        connect( nqsc, SIGNAL(finishedListing()),
                 this, SLOT(finishedListing()) );

        nqsc->query( query );
        m_finished++;
    }
}


void SharingApplication::newEntries(const QList< Nepomuk::Query::Result >& results )
{
    foreach( const Nepomuk::Query::Result& r, results ) {
        kDebug() << r.resource().resourceUri();
        if( !r.excerpt().isEmpty() )
            kDebug() << "Excerpt: " << r.excerpt();
        if( r.additionalBindings().count() )
            kDebug() << r.additionalBindings();
    }
}

void SharingApplication::finishedListing()
{
    m_finished--;

    if( m_finished <= 0 ) {
        quit();
    }
}


int main( int argc, char ** argv ) {
    KAboutData aboutData("nepomuk-sharing-test", 0, ki18n("NepomukSharingTest"),
                         "0.1",
                         ki18n("An application to test Nepomuk Metadata Sharing"),
                         KAboutData::License_LGPL_V2,
                         ki18n("(C) 2011, Vishesh Handa"));
    aboutData.addAuthor(ki18n("Vishesh Handa"), ki18n("Current maintainer"), "handa.vish@gmail.com");

    KCmdLineArgs::init(argc, argv, &aboutData);

    KCmdLineOptions options;
    options.add("+query", ki18n("The desktop query that should be sent"));

    KCmdLineArgs::addCmdLineOptions(options);

    // Application
    SharingApplication app( argc, argv );
    KComponentData data( aboutData, KComponentData::RegisterAsMainComponent );

    if( KCmdLineArgs::parsedArgs()->count() < 1 ) {
        kError() << "A query must be provided";
        return 1;
    }

    return app.exec();
}


#include "main.moc"
