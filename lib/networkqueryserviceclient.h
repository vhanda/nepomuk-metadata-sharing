/*
   This file is part of the Nepomuk KDE project.
   Copyright (C) 2011 Vishesh Handa <handa.vish@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) version 3, or any
   later version accepted by the membership of KDE e.V. (or its
   successor approved by the membership of KDE e.V.), which shall
   act as a proxy defined in Section 6 of version 3 of the license.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/



#ifndef NETWORKQUERYSERVICECLIENT_H
#define NETWORKQUERYSERVICECLIENT_H

#include "nepomuk_sharing_export.h"

#include <QtCore/QUrl>

#include <Nepomuk/Query/Result>

namespace Nepomuk {

    namespace Query {
        class Query;
    }

    class NEPOMUKSHARING_EXPORT NetworkQueryServiceClient : public QObject
    {
        Q_OBJECT
    public:
        NetworkQueryServiceClient( const QUrl& repositoryUri,
                                   QObject* parent=0 );
        virtual ~NetworkQueryServiceClient();

    public Q_SLOTS:
        bool query( const Nepomuk::Query::Query& query );

        /**
         * Close the client, thus stop to monitor the query
         * for changes. Without closing the client it will continue
         * signalling changes to the results.
         *
         * This will also make any blockingQuery return immediately.
         */
        void close();

        /**
         * \return \p true if all results have been listed (ie. finishedListing() has
         * been emitted), close() has been called, or no query was started.
         *
         * \since 4.6
         */
        bool isListingFinished() const;

        /**
         * The last error message which has been emitted via error() or an
         * empty string if there was no error.
         *
         * \since 4.6
         */
        QString errorMessage() const;

    Q_SIGNALS:
        /**
         * Emitted for new search results. This signal is emitted both
         * for the initial listing and for changes to the search.
         */
        void newEntries( const QList<Nepomuk::Query::Result>& entries );

        /**
         * Emitted if the search results changed when monitoring a query.
         * \param entries A list of resource URIs identifying the resources
         * that dropped out of the query results.
         */
        void entriesRemoved( const QList<QUrl>& entries );

        /**
         * The number of results that are reported via newEntries() before the
         * finishedListing() signal.
         *
         * Emitted once the count of the results is available. This might
         * happen before the first result is emitted, in between the results, or
         * in rare cases it could even happen after all results have been reported.
         *
         * Also be aware that no count will be provided when using sparqlQuery()
         *
         * \since 4.6
         */
        void resultCount( int count );

        /**
         * Emitted when the initial listing has been finished, ie. if all
         * results have been reported via newEntries. If no further updates
         * are necessary the client should be closed now.
         *
         * In case of an error this signal is not emitted.
         *
         * \sa error()
         */
        void finishedListing();

        /**
         * Emitted when an error occurs. This typically happens in case the query
         * service is not running or does not respond. No further signals will be
         * emitted after this one.
         *
         * \since 4.6
         */
        void error( const QString& errorMessage );

    private:
        class Private;
        Private* const d;

        Q_PRIVATE_SLOT( d, void _k_handleQueryReply(QDBusPendingCallWatcher*) )
        Q_PRIVATE_SLOT( d, void _k_finishedListing() )

    };

}
#endif // NETWORKQUERYSERVICECLIENT_H
