/*
   This file is part of the Nepomuk KDE project.
   Copyright (C) 2011 Vishesh Handa <handa.vish@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) version 3, or any
   later version accepted by the membership of KDE e.V. (or its
   successor approved by the membership of KDE e.V.), which shall
   act as a proxy defined in Section 6 of version 3 of the license.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "networkqueryserviceclient.h"
#include "queryinterface.h"
#include "metadataserviceinterface.h"
#include "../service/dbusoperators_p.h"

#include <QtDBus/QDBusPendingCallWatcher>
#include <QtDBus/QDBusPendingCall>
#include <QtDBus/QDBusPendingReplyData>
#include <QtDBus/QDBusConnection>


#include <KDebug>

class Nepomuk::NetworkQueryServiceClient::Private {
public:
    NetworkQueryServiceClient *q;

    QUrl m_repositoryUri;
    QDBusPendingCallWatcher* m_pendingCallWatcher;

    org::kde::nepomuk::Query *m_queryInterface;
    org::kde::nepomuk::MetadataSharing *m_metadataSharingInterface;

    void _k_handleQueryReply( QDBusPendingCallWatcher* );
    void _k_finishedListing();

    void createQueryInterface( const QString& path );
};


Nepomuk::NetworkQueryServiceClient::NetworkQueryServiceClient(const QUrl& repositoryUri, QObject* parent)
    : QObject(parent),
      d( new Nepomuk::NetworkQueryServiceClient::Private )
{
    d->q = this;
    d->m_repositoryUri = repositoryUri;

    Nepomuk::Query::registerDBusTypes();

    QLatin1String serviceName("org.kde.nepomuk.services.nepomukmetadatasharing");
    d->m_metadataSharingInterface = new org::kde::nepomuk::MetadataSharing( serviceName,
                                                                            "/nepomukmetadatasharing",
                                                                            QDBusConnection::sessionBus() );
}

Nepomuk::NetworkQueryServiceClient::~NetworkQueryServiceClient()
{
    close();

    delete d->m_queryInterface;
    delete d->m_metadataSharingInterface;
    delete d;
}

bool Nepomuk::NetworkQueryServiceClient::query(const Nepomuk::Query::Query& query)
{
    if ( d->m_metadataSharingInterface->isValid() ) {
        QDBusPendingCall call = d->m_metadataSharingInterface->asyncCall( QLatin1String("query"),
                                                                          d->m_repositoryUri.toString(),
                                                                          query.toString() );
        d->m_pendingCallWatcher = new QDBusPendingCallWatcher( call, this);
        connect(d->m_pendingCallWatcher, SIGNAL(finished(QDBusPendingCallWatcher*)),
                this, SLOT(_k_handleQueryReply(QDBusPendingCallWatcher*)));

        return true;
    }

    kDebug() << "Service not running";
    return false;
}

void Nepomuk::NetworkQueryServiceClient::Private::_k_handleQueryReply(QDBusPendingCallWatcher* watcher)
{
    QDBusPendingReply<QDBusObjectPath> reply = *watcher;
    if(reply.isError()) {
        kDebug() << "ERROR: " << reply.error().message();
    }
    else {
        QString path = reply.value().path();
        if( path.isEmpty() ) {
            kDebug() << "Empty path!";
            return;
        }

        createQueryInterface( path );
    }

    delete watcher;
}

void Nepomuk::NetworkQueryServiceClient::Private::_k_finishedListing()
{
    emit q->finishedListing();
}


void Nepomuk::NetworkQueryServiceClient::Private::createQueryInterface(const QString& path)
{
    kDebug() << "PATH: " << path;
    m_queryInterface = new org::kde::nepomuk::Query( "org.kde.nepomuk.services.nepomukmetadatasharing",
                                                     path,
                                                     QDBusConnection::sessionBus(), q );

    if( m_queryInterface->isValid() ) {
        kDebug() << "VALID";
    }

    connect( m_queryInterface, SIGNAL( newEntries( QList<Nepomuk::Query::Result> ) ),
             q, SIGNAL( newEntries( QList<Nepomuk::Query::Result> ) ) );
    connect( m_queryInterface, SIGNAL( resultCount(int) ),
             q, SIGNAL( resultCount(int) ) );
    connect( m_queryInterface, SIGNAL( finishedListing() ),
             q, SLOT( _k_finishedListing() ) );

    //connect( m_queryInterface, SIGNAL( entriesRemoved( QStringList ) ),
    //         q, SLOT( _k_entriesRemoved( QStringList ) ) );
    // run the listing async in case the event loop below is the only one we have
    // and we need it to handle the signals and list returns results immediately
    //QTimer::singleShot( 0, queryInterface, SLOT(list()) );
}


void Nepomuk::NetworkQueryServiceClient::close()
{
    if( d->m_queryInterface &&  d->m_queryInterface->isValid() )
        d->m_queryInterface->close();
}

QString Nepomuk::NetworkQueryServiceClient::errorMessage() const
{
    return QString();
}

bool Nepomuk::NetworkQueryServiceClient::isListingFinished() const
{
    return false;
}



#include "networkqueryserviceclient.moc"



