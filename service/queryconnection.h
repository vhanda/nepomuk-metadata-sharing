/*
   This file is part of the Nepomuk KDE project.
   Copyright (C) 2008-2010 Sebastian Trueg <trueg@kde.org>
   Copyright (C) 2011 Vishesh Handa <handa.vish@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) version 3, or any
   later version accepted by the membership of KDE e.V. (or its
   successor approved by the membership of KDE e.V.), which shall
   act as a proxy defined in Section 6 of version 3 of the license.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef QUERYCONNECTION_H
#define QUERYCONNECTION_H

#include "queryreply.h"

#include <QtCore/QObject>
#include <QtDBus/QDBusObjectPath>
#include <QtDBus/QDBusServiceWatcher>

namespace Nepomuk {

    class QueryConnection : public QObject
    {
        Q_OBJECT

    public:
        QueryConnection( QueryReply *reply, QObject *parent = 0 );

        QDBusObjectPath registerDBusObject( const QString& dbusClient, int id );

    public Q_SLOTS:
        /// List all entries in the folder, used by the public kdelibs API
        void list();

        /// internal API used by the kio kded module to listen for changes
        void listen();

        /// close the connection to the folder. Will delete this connection
        void close();

        /// \return \p true if the initial listing has been finished and the finishedListing()
        /// has been emitted
        bool isListingFinished() const;

        /// \return the SPARQL query of the folder
        QString queryString() const;

    Q_SIGNALS:
        void newEntries( const QList<Nepomuk::Query::Result>& );
        void entriesRemoved( const QStringList& );
        void entriesRemoved( const QList<Nepomuk::Query::Result>& entries );

        void resultCount( int count );
        void totalResultCount( int count );
        void finishedListing();

    private:
        QueryReply *m_reply;

        QDBusServiceWatcher *m_serviceWatcher;
    };

}
#endif // QUERYCONNECTION_H
