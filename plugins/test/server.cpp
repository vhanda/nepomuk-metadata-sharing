/*
   This file is part of the Nepomuk KDE project.
   Copyright (C) 2011 Vishesh Handa <handa.vish@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) version 3, or any
   later version accepted by the membership of KDE e.V. (or its
   successor approved by the membership of KDE e.V.), which shall
   act as a proxy defined in Section 6 of version 3 of the license.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "server.h"

#include <KDebug>
#include <QtCore/QXmlStreamWriter>

#include <Nepomuk/ResourceManager>
#include <Nepomuk/Resource>

#include <Nepomuk/Query/Query>
#include <Nepomuk/Query/Result>
#include <Nepomuk/Query/QueryServiceClient>

#include <Soprano/Model>
#include <Soprano/QueryResultIterator>
#include <Soprano/StatementIterator>
#include <Soprano/PluginManager>
#include <Soprano/Serializer>
#include <Soprano/Util/SimpleStatementIterator>

Nepomuk::Server::Server(QObject* parent)
    : QTcpServer(parent)
{
    kDebug() << "Listening for connections on port " << PortNumber;
    listen( QHostAddress::Any, PortNumber );

    connect( this, SIGNAL(newConnection()), this, SLOT(slotNewConnection()) );
}


QString Nepomuk::Server::toXml(const QList< Soprano::BindingSet >& bindings)
{
    if( bindings.isEmpty() )
        return QString();

    QString s;
    QXmlStreamWriter xml( &s );

    xml.writeStartDocument();
    xml.writeStartElement(QLatin1String("sparql"));
    //xml.writeNamespace("http://www.w3.org/2005/sparql-results#");

    xml.writeStartElement(QLatin1String("head"));
    // Add the additional variables
    foreach( const QString& var, bindings.first().bindingNames() ) {
        xml.writeStartElement("variable");
        xml.writeAttribute("name", var);
        xml.writeEndElement();
    }
    xml.writeEndElement(); // head

    //
    // Results
    //
    xml.writeStartElement("results");
    foreach( const Soprano::BindingSet& bs, bindings ) {
        xml.writeStartElement("result");

        foreach( const QString& binding, bs.bindingNames() ) {
            xml.writeStartElement("binding");
            xml.writeAttribute("name", binding);
            Soprano::Node node = bs.value( binding );
            if( node.isResource() ) {
                xml.writeStartElement("uri");
                xml.writeCharacters( node.uri().toString() );
                xml.writeEndElement();
            }
            else if( node.isLiteral() ) {
                xml.writeStartElement("literal");
                xml.writeAttribute("datatype", node.literal().dataTypeUri().toString() );
                xml.writeCharacters( node.literal().toString() );
                xml.writeEndElement();
            }
            xml.writeEndElement();
        }
        xml.writeEndElement(); // result
    }
    xml.writeEndElement(); // results
    xml.writeEndElement(); // sparql
    xml.writeEndDocument();

    return s;
}

static const char* s_scoreVarName = "_n_f_t_m_s_";
static const char* s_excerptVarName = "_n_f_t_m_ex_";

QString Nepomuk::Server::toXml(const QList< Nepomuk::Query::Result >& results)
{
    if( results.isEmpty() )
        return QString();

    QList<Soprano::BindingSet> bindings;
    bindings.reserve( results.size() );

    foreach( const Nepomuk::Query::Result& result, results ) {
        Soprano::BindingSet bs = result.additionalBindings();
        bs.insert( QLatin1String("r"), result.resource().resourceUri() );

        if( !result.excerpt().isEmpty() )
            bs.insert( s_excerptVarName, Soprano::LiteralValue( result.excerpt() ) );
        if( result.score() )
            bs.insert( s_scoreVarName, Soprano::LiteralValue( result.score() ) );

        QHash< Types::Property, Soprano::Node > requestProp = result.requestProperties();
        QHashIterator< Types::Property, Soprano::Node > it( requestProp );
        while( it.hasNext() ) {
            it.next();
            QString varName = QLatin1String("req-") + it.key().uri().toString();
            bs.insert( varName, it.value() );
        }

        bindings << bs;
    }

    return toXml( bindings );
}

QList< Soprano::BindingSet > Nepomuk::Server::fromXmlBindings(const QString& string)
{
    QList<Soprano::BindingSet> bindings;

    QXmlStreamReader xml( string );

    bool inResults = false;
    while( !xml.atEnd() ) {
        xml.readNext();
        if( !xml.isStartElement() )
            continue;
        //xml.readNextStartElement();

        if( xml.name() == "head" ) {
            xml.readNextStartElement();
            while ( xml.name() == "variable" ) {
                xml.readNextStartElement();
            }
            continue;
        }

        // vHanda: This is some weird logic
        if( xml.name() != "results" ) {
            inResults = true;
        }

        if( inResults ) {
            while( xml.name() == "result" ) {
                if( !xml.readNextStartElement() ) {
                    break;
                }

                Soprano::BindingSet set;
                while( xml.name() == "binding" ) {
                    QXmlStreamAttributes atrib = xml.attributes();
                    const QString varName = atrib.value( "name" ).toString();
                    xml.readNextStartElement();

                    Soprano::Node node;
                    QStringRef nodeType = xml.name();
                    if( nodeType == "uri" ) {
                        const QString uri = xml.readElementText();
                        node = Soprano::Node( QUrl(uri) );
                    }
                    else if( nodeType == "literal" ) {
                        QXmlStreamAttributes at = xml.attributes();
                        const QString dataType = at.value("datatype").toString();
                        const QString value = xml.readElementText();

                        node = Soprano::Node( Soprano::LiteralValue::fromString(value, dataType) );
                    }

                    set.insert( varName, node );

                    QXmlStreamReader::TokenType token = xml.readNext();
                    if( token != QXmlStreamReader::EndElement ) {
                        kDebug() << "Parsing error";
                        break;
                    }

                    xml.readNextStartElement();
                }

                if( set.count() )
                    bindings << set;
            }
        } // if( in results )
    }

    return bindings;
}

QList<Nepomuk::Query::Result> Nepomuk::Server::fromXml(const QString& string)
{
    QList<Nepomuk::Query::Result> results;

    QList< Soprano::BindingSet > bindings = fromXmlBindings( string );
    foreach( const Soprano::BindingSet &set, bindings ) {
        QStringList names = set.bindingNames();
        if( names.isEmpty() )
            continue;

        names.removeAll( QLatin1String("r") );

        Soprano::Node resNode = set.value(QLatin1String("r"));
        Nepomuk::Query::Result result( Nepomuk::Resource::fromResourceUri( resNode.uri() ) );

        Soprano::BindingSet newSet;
        int score = 0;
        Q_FOREACH( const QString& var, names ) {
            if ( var == QLatin1String( s_scoreVarName ) )
                score = set[var].literal().toInt();
            else if ( var == QLatin1String( s_excerptVarName ) )
                result.setExcerpt( set[var].toString() );
            else if ( var.startsWith("req-") ) {
                QUrl uri( var.mid( 4 ) );
                result.addRequestProperty( uri, set[var] );
            }
            else {
                newSet.insert( var, set[var] );
            }
        }

        result.setAdditionalBindings( newSet );
        result.setScore( score );

        results << result;
    }

    return results;
}


void Nepomuk::Server::slotNewConnection()
{
    kDebug() << "New connection";
    if( !hasPendingConnections() )
        return;

    QTcpSocket *socket = nextPendingConnection();

    QByteArray line;
    while( !line.endsWith( QByteArray("\r\n") ) ) {
        socket->waitForReadyRead();
        line.append( socket->readLine() );
    }
    kDebug() << line;

    //FIXME: Use ByteArray
    QStringList tokens = QString::fromUtf8(line).split( QRegExp(QLatin1String("[ \r\n]+")) );

    if( tokens.isEmpty() ) {
        kDebug() << "No tokens found";
        return;
    }

    if( tokens.size() < 2 ) {
        return;
    }

    if( tokens[0].toUpper() != QLatin1String("GET") ) {
        kDebug() << "We only handle GET requests";
        socket->close();
        return;
    }

    //FIXME: The QUrl constructor is absolute crap. Donot use it. Ever!
    const QUrl url = QUrl::fromEncoded( tokens[1].toUtf8() );
    const QString urlString = url.toString();

    if( !urlString.startsWith( QLatin1String("/nepomuk") ) ) {
        kDebug() << "We only handle nepomuk requests";
        socket->close();
        return;
    }

    // Queries
    if( url.hasQueryItem(QLatin1String("query")) ) {
        QString queryString = url.queryItemValue( QLatin1String("query") );
        kDebug() << "QueryString: " << queryString;

        // Perform the Query
        Nepomuk::Query::Query query = Query::Query::fromString( queryString );
        QList< Query::Result > results = Query::QueryServiceClient::syncQuery( query );
        QString resultString = toXml( results );

        kDebug() << "Writing the result : " << resultString;
        QTextStream out( socket );
        out << QString::fromLatin1("HTTP/1.1 200 OK\r\n");
        out << QString::fromLatin1("Content-Type: application/sparql-results+xml\r\n");
        out << QString::fromLatin1("Content-Length: %1\r\n").arg( resultString.size() );
        out << QString::fromLatin1("\r\n");
        out << resultString;
        out.flush();

        socket->waitForBytesWritten();
    }
    // Get
    else if( urlString.startsWith( QLatin1String("/nepomuk:/") ) ) {
        const QUrl resUri( url.path().mid(1) );

        Soprano::Model *model = Nepomuk::ResourceManager::instance()->mainModel();
        QList<Soprano::Statement> stList = model->listStatements( resUri, QUrl(), QUrl() ).allStatements();

        const Soprano::Serializer* serializer
            = Soprano::PluginManager::instance()->discoverSerializerForSerialization( Soprano::SerializationNQuads );

        Soprano::Util::SimpleStatementIterator it( stList );

        QString output;
        QTextStream stream( &output );
        if( !serializer->serialize( it, stream, Soprano::SerializationNQuads ) ) {
            kDebug() << "Serialization failed";
            socket->close();
            return;
        }

        QTextStream out( socket );
        out << QString::fromLatin1("HTTP/1.1 200 OK\r\n");
        out << QString::fromLatin1("Content-Type: text/x-nquads\r\n");
        out << QString::fromLatin1("Content-Length: %1\r\n").arg( output.size() );
        out << QString::fromLatin1("\r\n");
        out << output;
        out.flush();

        socket->waitForBytesWritten();
    }


    kDebug() << "request url not recognized " << url;
    socket->close();
}
