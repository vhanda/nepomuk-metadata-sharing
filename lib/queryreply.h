/*
   This file is part of the Nepomuk KDE project.
   Copyright (C) 2011 Vishesh Handa <handa.vish@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) version 3, or any
   later version accepted by the membership of KDE e.V. (or its
   successor approved by the membership of KDE e.V.), which shall
   act as a proxy defined in Section 6 of version 3 of the license.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef NEPOMUK_QUERY_REPLY_H
#define NEPOMUK_QUERY_REPLY_H

#include "nepomuk_sharing_export.h"

#include <Nepomuk/Query/Result>

namespace Nepomuk {

    /**
     * Each plugin should should create its own QueryReply
     */
    class NEPOMUKSHARING_EXPORT QueryReply : public QObject
    {
        Q_OBJECT
    public:
        QueryReply(QObject *parent=0);
        virtual ~QueryReply();

    Q_SIGNALS:
        /**
         * Emitted for new search results. This signal is emitted both
         * for the initial listing and for changes to the search.
         */
        void newEntries( const QList<Nepomuk::Query::Result>& entries );

        /**
         * The number of results that are reported via newEntries() before the
         * finishedListing() signal.
         *
         * Emitted once the count of the results is available. This might
         * happen before the first result is emitted, in between the results, or
         * in rare cases it could even happen after all results have been reported.
         */
        void resultCount( int count );

        /**
         * Emitted when the initial listing has been finished, ie. if all
         * results have been reported via newEntries. If no further updates
         * are necessary the client should be closed now.
         *
         * In case of an error this signal is not emitted.
         *
         * \sa error()
         */
        void finishedListing();

        /**
         * Emitted when an error occurs. This typically happens in case the query
         * service is not running or does not respond. No further signals will be
         * emitted after this one.
         *
         */
        void error( const QString& errorMessage );

    public Q_SLOTS:
        virtual bool isListingFinished() = 0;
    };
}

#endif