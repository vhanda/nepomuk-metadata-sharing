/*
   This file is part of the Nepomuk KDE project.
   Copyright (C) 2011 Vishesh Handa <handa.vish@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) version 3, or any
   later version accepted by the membership of KDE e.V. (or its
   successor approved by the membership of KDE e.V.), which shall
   act as a proxy defined in Section 6 of version 3 of the license.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "metadatarequestjob.h"
#include "metadataserviceinterface.h"
#include "metadatareplyinterface.h"

#include <Nepomuk/Resource>
#include <Nepomuk/ResourceManager>

#include <QtDBus/QDBusPendingCallWatcher>
#include <QtDBus/QDBusPendingCall>
#include <QtDBus/QDBusPendingReplyData>
#include <QtDBus/QDBusConnection>

#include <KDebug>

class Nepomuk::MetadataRequestJob::Private {
public:
    Nepomuk::MetadataRequestJob *q;

    QUrl resourceUri;
    QUrl repositoryUri;
    QUrl newResourceUri;

    QDBusPendingCallWatcher* m_pendingCallWatcher;

    org::kde::nepomuk::MetadataReply *m_replyInterface;
    org::kde::nepomuk::MetadataSharing *m_metadataSharingInterface;

    void _k_handleReply( QDBusPendingCallWatcher* );
    void _k_done( const QString& resUri );
};

Nepomuk::MetadataRequestJob::MetadataRequestJob(const QUrl& resourceUri, const QUrl& repoUri, QObject* parent)
    : KJob(parent),
      d( new Nepomuk::MetadataRequestJob::Private )
{
    d->q = this;
    d->resourceUri = resourceUri;
    d->repositoryUri = repoUri;

    QLatin1String serviceName("org.kde.nepomuk.services.nepomukmetadatasharing");
    d->m_metadataSharingInterface = new org::kde::nepomuk::MetadataSharing( serviceName,
                                                                            "/nepomukmetadatasharing",
                                                                            QDBusConnection::sessionBus() );
}

Nepomuk::MetadataRequestJob::~MetadataRequestJob()
{
    delete d->m_metadataSharingInterface;
    delete d;
}

void Nepomuk::MetadataRequestJob::start()
{
    if ( d->m_metadataSharingInterface->isValid() ) {
        QDBusPendingCall call = d->m_metadataSharingInterface->asyncCall( QLatin1String("fetch"),
                                                                          d->repositoryUri.toString(),
                                                                          d->resourceUri.toString() );
        d->m_pendingCallWatcher = new QDBusPendingCallWatcher( call, this );
        connect(d->m_pendingCallWatcher, SIGNAL(finished(QDBusPendingCallWatcher*)),
                this, SLOT(_k_handleReply(QDBusPendingCallWatcher*)));

        // Set some error
        return;
    }

    // Set some error
    return;
}

void Nepomuk::MetadataRequestJob::Private::_k_handleReply(QDBusPendingCallWatcher* watcher)
{
    QDBusPendingReply<QDBusObjectPath> reply = *watcher;
    if(reply.isError()) {
        kDebug() << "ERROR: " << reply.error().message();
    }
    else {
        QString path = reply.value().path();
        if( path.isEmpty() ) {
            kDebug() << "Empty path!";
            return;
        }

        m_replyInterface = new org::kde::nepomuk::MetadataReply( "org.kde.nepomuk.services.nepomukmetadatasharing",
                                                                 path,
                                                                 QDBusConnection::sessionBus(), q );

        if( m_replyInterface->isValid() ) {
            kDebug() << "VALID";
        }

        connect( m_replyInterface, SIGNAL(finished(QString)), q, SLOT(_k_done(QString)) );
    }

    delete watcher;
}

void Nepomuk::MetadataRequestJob::Private::_k_done(const QString& resUri)
{
    newResourceUri = resUri;
    q->emitResult();
}

Nepomuk::Resource Nepomuk::MetadataRequestJob::resource()
{
    return Resource::fromResourceUri( d->newResourceUri );
}






