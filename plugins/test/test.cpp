/*
   This file is part of the Nepomuk KDE project.
   Copyright (C) 2011 Vishesh Handa <handa.vish@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) version 3, or any
   later version accepted by the membership of KDE e.V. (or its
   successor approved by the membership of KDE e.V.), which shall
   act as a proxy defined in Section 6 of version 3 of the license.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "test.h"
#include "server.h"
#include "nso.h"
#include "testqueryreply.h"

#include <KDebug>
#include <QtNetwork/QHostAddress>

#include <KDE/DNSSD/ServiceBrowser>

#include <Soprano/Model>
#include <Soprano/QueryResultIterator>

#include <Nepomuk/ResourceManager>
#include <Nepomuk/Resource>
#include <Nepomuk/Variant>

using namespace Nepomuk::Vocabulary;

static const char *s_dnsType = "_nepomuk._tcp";

Nepomuk::Test::Test(QObject* parent, const QList< QVariant >& args)
    : MetadataTransferPlugin(parent)
{
    Q_UNUSED( args );
    kDebug();

    m_dnsService = new DNSSD::PublicService( QString(),
                                             QLatin1String(s_dnsType),
                                             Server::PortNumber );

    QString query = QString::fromLatin1("select ?ident where { ?r a %1 . ?r %2 ?ident . ?r %3 %4 }")
                    .arg( Soprano::Node::resourceToN3( NSO::Repository() ),
                          Soprano::Node::resourceToN3( NSO::repositoryIdentifier() ),
                          Soprano::Node::resourceToN3( NSO::belongsTo() ),
                          Soprano::Node::resourceToN3( QUrl("nepomuk:/myself") ) );

    Soprano::Model *model = Nepomuk::ResourceManager::instance()->mainModel();
    Soprano::QueryResultIterator it = model->executeQuery( query, Soprano::Query::QueryLanguageSparql );
    QString uuid;
    if( it.next() ) {
        uuid = it["ident"].literal().toString();
    }

    kDebug() << "UUID: " << uuid;

    QMap<QString, QByteArray> textData;
    textData.insert( QLatin1String("uuid"), uuid.toUtf8() );
    //FIXME: Add the PIMO:Person information as well
    m_dnsService->setTextData( textData );

    kDebug() << "Publishing zeroconf service";
    connect( m_dnsService, SIGNAL(published(bool)), this, SLOT(published(bool)) );
    m_dnsService->publishAsync();
}

void Nepomuk::Test::published(bool pub)
{
    if( !pub ) {
        kDebug() << "The DNS SD could not be published.";
        return;
    }
    kDebug() << "Published";

    DNSSD::ServiceBrowser *browser = new DNSSD::ServiceBrowser(QLatin1String(s_dnsType), true);
    connect(browser, SIGNAL(serviceAdded(DNSSD::RemoteService::Ptr)),
            this, SLOT(addService(DNSSD::RemoteService::Ptr)));
    connect(browser, SIGNAL(serviceRemoved(DNSSD::RemoteService::Ptr)),
            this, SLOT(delService(DNSSD::RemoteService::Ptr)));
    browser->startBrowse();
}

bool Nepomuk::Test::canHandle(const QUrl& uri)
{
    kDebug() << uri << m_onlineRepos.contains(uri);
    if( m_onlineRepos.contains(uri) )
        return true;

    return false;
}

Nepomuk::QueryReply* Nepomuk::Test::query(const QUrl& uri, const Nepomuk::Query::Query& query)
{
    QString hostName = m_onlineRepos.value( uri );
    kDebug() << "Querying repo uri " << uri << " on " << hostName;

    QueryReply *reply = new TestQueryReply( hostName, query, this );
    return reply;
}

Nepomuk::MetadataReply* Nepomuk::Test::fetch(const QUrl& uri, const QUrl& resourceUri)
{
    return 0;
}


void Nepomuk::Test::addService(DNSSD::RemoteService::Ptr ptr)
{
    if( !ptr->isResolved() )
        ptr->resolve();

    QMap<QString, QByteArray> textData = ptr->textData();
    const QString uuid = QString::fromUtf8( textData.value(QLatin1String("uuid")) );

    QUrl repoUri = repositoryForUuid( uuid );

    // Create the repository
    if( repoUri.isEmpty() ) {
        Nepomuk::Resource repo;
        repo.addType( NSO::Repository() );
        repo.setProperty( NSO::repositoryIdentifier(), Nepomuk::Variant( uuid ) );
        //FIXME: Add the PIMO person as well

        repoUri = repo.resourceUri();
    }

    kDebug() << "Adding " << repoUri << " " << ptr->hostName();
    m_onlineRepos.insert( repoUri, ptr->hostName() );
}

void Nepomuk::Test::delService(DNSSD::RemoteService::Ptr ptr)
{
    QMap<QString, QByteArray> textData = ptr->textData();
    const QString uuid = QString::fromUtf8( textData.value(QLatin1String("uuid")) );

    QUrl repoUri = repositoryForUuid( uuid );

    kDebug() << "Removing " << repoUri;
    m_onlineRepos.remove( repoUri );
}

QUrl Nepomuk::Test::repositoryForUuid(const QString& uuid)
{
    // Check if it already exists
    QString query = QString::fromLatin1("select ?r where { ?r %1 %2 }")
                    .arg( Soprano::Node::resourceToN3( NSO::repositoryIdentifier() ),
                          Soprano::Node::literalToN3( uuid ) );

    Soprano::Model *model = Nepomuk::ResourceManager::instance()->mainModel();
    Soprano::QueryResultIterator it = model->executeQuery( query, Soprano::Query::QueryLanguageSparql );
    if( it.next() ) {
        return it["r"].uri();
    }

    return QUrl();
}



#include <kpluginfactory.h>
#include <kpluginloader.h>

NEPOMUK_METADATASHARING_PLUGIN_EXPORT( Nepomuk::Test, "nepomukmetadatasharing-test" )