/*
   This file is part of the Nepomuk KDE project.
   Copyright (C) 2008-2010 Sebastian Trueg <trueg@kde.org>
   Copyright (C) 2011 Vishesh Handa <handa.vish@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) version 3, or any
   later version accepted by the membership of KDE e.V. (or its
   successor approved by the membership of KDE e.V.), which shall
   act as a proxy defined in Section 6 of version 3 of the license.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "queryconnection.h"
#include "queryadaptor.h"

#include <KDebug>
#include <Nepomuk/Resource>

Nepomuk::QueryConnection::QueryConnection( QueryReply *reply, QObject *parent )
    : QObject(parent),
      m_reply( reply )
{
    // Connect all the signals
    connect( reply, SIGNAL(newEntries(QList<Nepomuk::Query::Result>)),
             this, SIGNAL(newEntries(QList<Nepomuk::Query::Result>)) );

    connect( reply, SIGNAL(resultCount(int)), this, SIGNAL(resultCount(int)) );
    connect( reply, SIGNAL(resultCount(int)), this, SIGNAL(totalResultCount(int)) );
    connect( reply, SIGNAL(finishedListing()), this, SIGNAL(finishedListing()) );
}


QDBusObjectPath Nepomuk::QueryConnection::registerDBusObject(const QString& dbusClient, int id)
{
    new QueryAdaptor( this );

    // build the dbus object path from the id and register the connection as a Query dbus object
    const QString dbusObjectPath = QString::fromLatin1( "/query%1" ).arg( id );
    QDBusConnection::sessionBus().registerObject( dbusObjectPath, this );

    // watch the dbus client for unregistration for auto-cleanup
    m_serviceWatcher = new QDBusServiceWatcher( dbusClient,
                                                QDBusConnection::sessionBus(),
                                                QDBusServiceWatcher::WatchForUnregistration,
                                                this );
    connect( m_serviceWatcher, SIGNAL(serviceUnregistered(QString)),
             this, SLOT(close()) );

    // finally return the dbus object path this connection can be found on
    return QDBusObjectPath( dbusObjectPath );
}


void Nepomuk::QueryConnection::close()
{
    deleteLater();
}

bool Nepomuk::QueryConnection::isListingFinished() const
{
    return m_reply->isListingFinished();
}

void Nepomuk::QueryConnection::list()
{

}

void Nepomuk::QueryConnection::listen()
{

}

QString Nepomuk::QueryConnection::queryString() const
{
    return QString();
}
