/*
   This file is part of the Nepomuk KDE project.
   Copyright (C) 2011 Vishesh Handa <handa.vish@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) version 3, or any
   later version accepted by the membership of KDE e.V. (or its
   successor approved by the membership of KDE e.V.), which shall
   act as a proxy defined in Section 6 of version 3 of the license.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NEPOMUK_METADATASHARING_SERVICE_H
#define NEPOMUK_METADATASHARING_SERVICE_H

#include <QtDBus/QDBusObjectPath>
#include <QtDBus/QDBusMessage>

#include <Nepomuk/Service>
#include "metadatatransferplugin.h"

namespace Nepomuk {

    class MetadataSharing : public Service
    {
        Q_OBJECT
        Q_CLASSINFO( "D-Bus Interface", "org.kde.nepomuk.MetadataSharing" )

    public:
        MetadataSharing( QObject * parent = 0, const QList<QVariant>& args = QList<QVariant>() );
        ~MetadataSharing();

    public Q_SLOTS:
        Q_SCRIPTABLE QDBusObjectPath query( const QString& repositoryUri,
                                            const QString& query, const QDBusMessage& msg  );

        Q_SCRIPTABLE QDBusObjectPath fetch( const QString& repositoryUri,
                                            const QString& resourceUri, const QDBusMessage& msg );
    private:
        void createRepository();
        void loadAllPlugins();

        MetadataTransferPlugin* m_testPlugin;

        int m_connectionCount;
    };

}

#endif // NEPOMUK_METADATASHARING_SERVICE_H
