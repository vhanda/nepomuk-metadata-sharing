/*
   This file is part of the Nepomuk KDE project.
   Copyright (C) 2011 Vishesh Handa <handa.vish@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) version 3, or any
   later version accepted by the membership of KDE e.V. (or its
   successor approved by the membership of KDE e.V.), which shall
   act as a proxy defined in Section 6 of version 3 of the license.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef METADATAREPLYCONNECTION_H
#define METADATAREPLYCONNECTION_H

#include <QtDBus/QDBusObjectPath>
#include <QtDBus/QDBusServiceWatcher>
#include "metadatareply.h"

class KJob;

namespace Nepomuk {

    class MetadataReplyConnection : public QObject
    {
        Q_OBJECT
    public:
        MetadataReplyConnection( MetadataReply *reply, const QUrl &repoUri, QObject *parent=0 );
        virtual ~MetadataReplyConnection();

    public slots:
        QDBusObjectPath registerDBusObject( const QString& dbusClient, int id );

    signals:
        void finished(const QString &resUri);

    private slots:
        void newResources(const QList<Soprano::Statement> &stList);
        void slotJobFinished(KJob *job);

    private:
        MetadataReply *m_reply;
        QDBusServiceWatcher *m_serviceWatcher;
        QUrl m_repositoryUri;
    };

}

#endif // METADATAREPLYCONNECTION_H
