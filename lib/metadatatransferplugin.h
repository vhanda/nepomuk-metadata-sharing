/*
   This file is part of the Nepomuk KDE project.
   Copyright (C) 2011 Vishesh Handa <handa.vish@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) version 3, or any
   later version accepted by the membership of KDE e.V. (or its
   successor approved by the membership of KDE e.V.), which shall
   act as a proxy defined in Section 6 of version 3 of the license.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef NEPOMUK_METADATASHARING_PLUGIN_H
#define NEPOMUK_METADATASHARING_PLUGIN_H

#include "nepomuk_sharing_export.h"
#include "queryreply.h"
#include "metadatareply.h"

namespace Nepomuk {

    namespace Query {
        class Query;
    };

    class NEPOMUKSHARING_EXPORT MetadataTransferPlugin : public QObject
    {
        Q_OBJECT
    public:
        MetadataTransferPlugin(QObject *parent=0);
        virtual ~MetadataTransferPlugin();

        /**
         * Indicates if the plugin can handle the Repository or Person that has the
         * URI \p uri.
         *
         * \return \c true if can handle
         * \return \c false if cannot
         */
        virtual bool canHandle( const QUrl& uri ) = 0;

        virtual QueryReply* query( const QUrl& uri, const Query::Query& query ) = 0;

        virtual MetadataReply* fetch( const QUrl& uri, const QUrl& resourceUri ) = 0;
    };
}

#include <kdemacros.h>
#include <KPluginFactory>
#include <KPluginLoader>

#define NEPOMUK_METADATASHARING_PLUGIN_EXPORT( classname, libname )    \
K_PLUGIN_FACTORY(factory, registerPlugin<classname>();) \
K_EXPORT_PLUGIN(factory(#libname))

/*
#define NEPOMUK_METADATASHARING_PLUGIN_EXPORT( c ) \
K_PLUGIN_FACTORY( MetadataSharingFactory, registerPlugin< c >(); ) \
K_EXPORT_PLUGIN( MetadataSharingFactory("c") )*/

#endif // NEPOMUK_METADATASHARING_PLUGIN_H
