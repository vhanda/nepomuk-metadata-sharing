/*
   This file is part of the Nepomuk KDE project.
   Copyright (C) 2011 Vishesh Handa <handa.vish@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) version 3, or any
   later version accepted by the membership of KDE e.V. (or its
   successor approved by the membership of KDE e.V.), which shall
   act as a proxy defined in Section 6 of version 3 of the license.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "testmetadatareply.h"
#include "server.h"

#include <KDebug>

#include <Soprano/StatementIterator>
#include <Soprano/PluginManager>
#include <Soprano/Parser>

TestMetadataReply::TestMetadataReply(const QString& hostname, const QUrl& resUri, QObject* parent)
    : MetadataReply(parent)
{
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(slotReplyFinished(QNetworkReply*)));

    QUrl url;
    url.setScheme(QLatin1String("http"));
    url.setHost(hostname);
    url.setPort(Nepomuk::Server::PortNumber);
    url.setPath(resUri.toString());

    QNetworkRequest request( url );
    m_reply = manager->get( request );

    connect( m_reply, SIGNAL(readyRead()), this, SLOT(slotReadReady()) );
}

void TestMetadataReply::slotReadReady()
{
    QByteArray data = m_reply->readAll();
    m_serializedStatements.append( QString::fromUtf8(data) );
}

void TestMetadataReply::slotReplyFinished(QNetworkReply* reply)
{
    if( reply->error() != QNetworkReply::NoError ) {
        kDebug() << reply->errorString();
        return;
    }

    // Read whatever is remaining
    slotReadReady();

    const Soprano::Parser *parser = Soprano::PluginManager::instance()->discoverParserForSerialization( Soprano::SerializationNQuads );
    Soprano::StatementIterator stIter = parser->parseString( m_serializedStatements, QUrl(), Soprano::SerializationNQuads );

    emit newResources( stIter.allStatements() );

    m_reply = 0;
}

